class Register:
    def __init__(self, size):
        self.size = size
        self.value = 0

    def get_number_base(self, input_string):
        number_base = 10
        if input_string[-1] == 'h':
            number_base = 16
        elif input_string[-1] == 'b':
            number_base = 2

        return number_base

    def convert_to_int(self, value, number_base):
        int_value = 0
        if number_base == 10:
            int_value = int(value)
        elif number_base == 16:
            int_value = int("0x" + value[:-1], 16)
        elif number_base == 2:
            int_value = int(value[:-1], 2)

        return int_value

    def mov(self, value):
        number_base = self.get_number_base(value)
        int_value = self.convert_to_int(value, number_base)
        if int_value >= 2**self.size:
            self.value = 2**self.size - 1
        else:
            self.value = int_value

    def add(self, value):
        number_base = self.get_number_base(value)
        int_value = self.convert_to_int(value, number_base)
        value = self.value + int_value
        if int(value) >= 2**self.size:
            self.value = 2**self.size - 1
        else:
            self.value = int_value

    def sub(self, value):
        number_base = self.get_number_base(value)
        int_value = self.convert_to_int(value, number_base)
        value = self.value - int_value
        if value <= 0:
            self.value = 0
        else:
            self.value = int_value

    def get_value(self):
        return f'{self.value:0{self.size}b}'