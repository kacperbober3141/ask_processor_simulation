from SM import Ui_MainWindow

from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QTextEdit
from PyQt5.QtCore import QEvent
from PyQt5.QtGui import QTextCursor
from master_register import MasterRegister
import sys
import datetime
import threading

class SMWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, *args, obj=None, **kwargs):
        super(SMWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)

        # guziki
        self.save_button.clicked.connect(self.save_instructions)
        self.load_button.clicked.connect(self.load_instructions)
        self.execute_button.clicked.connect(self.execute_program)
        self.debug_button.clicked.connect(self.debug_instructions)
        self.next_step_button.clicked.connect(self.next_step_instructions)

        self.input_edit.installEventFilter(self)
        self.num_of_line = 1
        self.first_line_text = True

        # stot
        self.stack_pointer = 16
        self.stack = [0] * self.stack_pointer

        self.debug_line = 0
        self.code_lines = []
        # rejestry
        self.register_size = 16
        self.A = MasterRegister(self.register_size)
        self.B = MasterRegister(self.register_size)
        self.C = MasterRegister(self.register_size)
        self.D = MasterRegister(self.register_size)

    def save_instructions(self):
        text = str(self.input_edit.toPlainText())
        f = open("session.txt", "w")
        f.write(text)
        f.close()

    def load_instructions(self):
        f = open("session.txt", "r")
        text = f.read()
        self.input_edit.setPlainText(text)

    def debug_instructions(self):
        code_text = self.get_input_text()
        self.code_lines = code_text.splitlines()
        self.debug_line = 0
        self.input_edit.zerowykursor()

    def next_step_instructions(self):
        if self.debug_line < len(self.code_lines):
            self.execute_line_code(self.code_lines[self.debug_line])
            if self.debug_line != 0:
                self.input_edit.nastepnyKursor()
            self.debug_line += 1

    def execute_line_code(self, line):
        instruction, destination, source = self.get_instruction_info(line)
        if instruction == 'mov':
            write_value = self.get_writing_value(source)
            self.choose_register(destination).mov(str(write_value))
        elif instruction == 'add':
            write_value = self.get_writing_value(source)
            self.choose_register(destination).add(str(write_value))
        elif instruction == 'sub':
            write_value = self.get_writing_value(source)
            self.choose_register(destination).sub(str(write_value))
        elif instruction[:3] == 'int':
            self.handle_interrupts(instruction)
        elif instruction == 'push':
            self.stack_pointer -= 1
            self.stack[self.stack_pointer] = self.get_writing_value(source)
            self.label_stackpointer.setText("SP: " + str(self.stack_pointer))
        elif instruction == 'pop':
            self.choose_register(source).mov(str(self.stack[self.stack_pointer]))
            self.stack_pointer += 1
            self.label_stackpointer.setText("SP: " + str(self.stack_pointer))
        else:
            print("BLEDNA INSTRUKCJA")
        self.update_registers()
        self.update_register_text()

    def execute_program(self):
        code_text = self.get_input_text()
        code_lines = code_text.splitlines()

        for line in code_lines:
            self.execute_line_code(line)

    def get_input(self):
        key = input("Podaj znak: ")
        print(key)
        self.A.regL.mov(str(ord(key)))


    def handle_interrupts(self, interrupt):
        if interrupt[-3:] == '14h':
            interrupt_program = hex(int(self.A.regH.get_value(), 2))
            port_number = int(self.D.get_value(), 2)
            if interrupt_program == "0x0":  # inicjalizacja portu
                communication_parameters = self.A.regL.get_value()
                print(f"Initialize Serial Port Parameters on port COM{port_number} with parameters: " + communication_parameters)
                self.textEdit.append("\nINT 14h,  00h (00)       Initialize Serial Port Parameters")
                self.textEdit.append("Initializes the baud rate, parity, stop-bit, and word length parameters for a serial port, and returns the status for the port.\n" +
                                     "On entry:\tAH\t00h\n" +
                                     "AL\tCommunications parameters bit(7, 6, 5) - baud rate (4,3) - prarity (2) Stop bits (1, 0) Word length\n" +
                                     "DX\tSerial port number (0 - COM1, 1 - COM2, etc.)\n" +
                                     "Returns:\tAH\tLine status (see Service 03h))")
            elif interrupt_program == "0x1":
                char_sent = self.A.regL.get_value()
                print(f"Send Character to Communications Port COM{port_number} character: " + char_sent)
                self.textEdit.append("\nINT 14h,  01h (1)        Send One Character")
                self.textEdit.append("Sends one character to the specified serial port.\n" +
                                     "On entry:\tAH\t01h\n" +
                                     "AL\tCharacter\n" +
                                     "DX\tSerial port number (0 - COM1, 1 - COM2, etc.)\n" +
                                     "Returns:\tAH\tLine status (see Service 03h))")
                self.A.regH.mov('01111110b')
            elif interrupt_program == "0x2":
                self.A.regL.mov('00111000b')
                char_rec = self.A.regL.get_value()
                print(f"Received Character from Communications Port COM{port_number} character: " + char_rec)
                self.textEdit.append("\nINT 14h,  02h (1)        Receive One Character")
                self.textEdit.append("Receives one character at the specified serial port.\n" +
                                     "On entry:\tAH\t02h\n" +
                                     "DX\tSerial port number (0 - COM1, 1 - COM2, etc.)\n" +
                                     "Returns:\tAH\tLine status (see Service 03h))" +
                                     "\tAL\tCharacter\n")
                self.A.regH.mov('01111110b')
        elif interrupt[-3:] == '21h':
            interrupt_program = hex(int(self.A.regH.get_value(), 2))
            if interrupt_program == "0x1":
                self.textEdit.append("\nINT 21h,  01h (1)        Read character from standard input")
                self.textEdit.append("read character from standard input, with echo,\n" +
                                     "On entry:\tAH\t01h\n" +
                                     "Returns:\tAL\tCharacter\n")
                QtWidgets.QApplication.processEvents()

                thread = threading.Thread(target=self.get_input)
                thread.start()

            elif interrupt_program == "0x2":
                self.textEdit.append("\nINT 21h,  02h (1)        Display Output")
                self.textEdit.append("Write character to standard output\n" +
                                     "On entry:\tAH\t02h\n" +
                                     "DL\tcharacter to write\n")
                output_char = int(self.D.regL.get_value(), 2)
                print(chr(output_char))
            elif interrupt_program == "0x4c":
                self.textEdit.append("\nINT 21h,  4Ch Terminate Process With Return Code")
                self.textEdit.append("Terminate Process With Return Code\n" +
                                     "On entry:\tAH\t4Ch\n" +
                                     "AL\t Return code\n")
                self.close()
                sys.exit(int(self.A.regL.get_value()))

            elif interrupt_program == "0xe":
                self.textEdit.append("\nINT 21h,  E - Select Disk")
                self.textEdit.append("Select Disk\n" +
                                     "On entry:\tAH\t0eh\n" +
                                     "DL\tzero based, drive number\n" +
                                     "Returns:\tAL\tone based, total number of logical drives including hardfiles\n")
                print(f"Selected disk: {int(self.D.regL.get_value(), 2)}")
                value = int(self.D.regL.get_value(), 2) + 1
                self.A.regL.mov(str(value))
            elif interrupt_program == "0x2a":
                now = datetime.datetime.now()

                self.A.regL.mov(str(now.weekday()))
                self.C.mov(str(now.year))
                self.D.regH.mov(str(now.month))
                self.D.regL.mov(str(now.day))

                self.textEdit.append(f"\nINT 21h,  2Ah (1)        Get date {now.date()}")
                self.textEdit.append("Write date to\n" +
                                     "AL = day of the week (0=Monday)\n" +
                                     "CX = year (1980-2099)\n" +
                                     "DH = month (1-12)\n" +
                                     "DL = day (1-31)\n")
            elif interrupt_program == "0x2c":
                now = datetime.datetime.now()

                self.C.regH.mov(str(now.hour))
                self.C.regL.mov(str(now.minute))
                self.D.regH.mov(str(now.second))
                self.D.regL.mov(str(now.microsecond))

                self.textEdit.append(f"\nINT 21h,  2Ch (1)        Get time {now.time()}")
                self.textEdit.append("Write time to\n" +
                                     "CH = hour (0-23)\n" +
                                     "CL = minutes (0-59)\n" +
                                     "DH = seconds (0-59)\n" +
                                     "DL = hundredths (0-99)\n")

            elif interrupt_program == "0x0d":
                self.textEdit.append(f"\nINT 21h,  0Dh (1)       Disk Reset")

    def update_registers(self):
        self.A.update_register()
        self.B.update_register()
        self.C.update_register()
        self.D.update_register()

    #def reset_program(self):


    def update_register_text(self):
        self.AH_edit.setText(self.A.regH.get_value())
        self.AL_edit.setText(self.A.regL.get_value())
        self.BH_edit.setText(self.B.regH.get_value())
        self.BL_edit.setText(self.B.regL.get_value())
        self.CH_edit.setText(self.C.regH.get_value())
        self.CL_edit.setText(self.C.regL.get_value())
        self.DH_edit.setText(self.D.regH.get_value())
        self.DL_edit.setText(self.D.regL.get_value())

    # value from register or instruction
    def get_writing_value(self, source):
        writing_value = 0
        register = self.choose_register(source)
        if not register:
            writing_value = source
        else:
            writing_value = register.value

        return writing_value

    def choose_register(self, destination):
        if destination == 'ax':
            return self.A
        elif destination == 'ah':
            return self.A.regH
        elif destination == 'al':
            return self.A.regL
        elif destination == 'bx':
            return self.B
        elif destination == 'bh':
            return self.B.regH
        elif destination == 'bl':
            return self.B.regL
        elif destination == 'cx':
            return self.C
        elif destination == 'ch':
            return self.C.regH
        elif destination == 'cl':
            return self.C.regL
        elif destination == 'dx':
            return self.D
        elif destination == 'dh':
            return self.D.regH
        elif destination == 'dl':
            return self.D.regL
        else:
            return False

    def get_input_text(self):
        text = str(self.input_edit.toPlainText())
        return text

    # noinspection PyMethodMayBeStatic
    def get_instruction_info(self, code_line):
        code_line = code_line.lower().strip()
        split_text = code_line.split(" ", 1)
        try:
            if len(split_text) > 1:
                instruction = split_text[0]
                destination, source = split_text[1].split(",")
            else:
                instruction = split_text[0]
                destination = ''
                source = ''
        except ValueError:
            instruction = split_text[0]
            source = split_text[-1]
            destination = ''

        return instruction, destination.strip(), source.strip()


if __name__ == "__main__":
    app = QApplication([])
    window = SMWindow()
    window.show()
    app.exec()
