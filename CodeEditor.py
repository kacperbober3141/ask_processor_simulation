import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QMainWindow, QApplication, QTextEdit, QWidget, QPlainTextEdit

import numpy as np


# klasa pomocnicza, aby rozrozniac linie i sie po nich poruszac
class LineNumberArea(QWidget):
    def __init__(self, editor):
        super().__init__(editor)
        self.editor = editor
        
    # nadpisanie metody sizeHint, ktora zwraca szerokosc numeracji linii
    def sizeHint(self):
        return Qsize(self.editor.lineNumberAreaWidth(), 0)

    # nadpisanie metody paintEvent wlasna implementacja zawarta w CodeEditor
    # sluzy to wyrysowaniu danego regionu w tym przypadku numeracji linii
    def paintEvent(self, event):
        self.editor.lineNumberAreaPaintEvent(event)


class CodeEditor(QPlainTextEdit):
    def __init__(self):
        super().__init__()
        
        # tworzymy obiekt odpowiedzialny za szerokosci numeracji linii oraz rysowanie numerow
        self.lineNumberArea = LineNumberArea(self)

        # poszerzenie linii w przypadku zwiekszenia ilosci cyfr w numeracji linii
        self.blockCountChanged.connect(self.updateLineNumberAreaWidth)
        
        # podczas przewijania 
        self.updateRequest.connect(self.updateLineNumberArea)
        
        # zaznacza na zolto linie w ktorej znajduje sie kursor
        self.cursorPositionChanged.connect(self.highlightCurrentLine)

        self.updateLineNumberAreaWidth(0)

    # metoda zwraca szerokosc linii numeracji  
    def lineNumberAreaWidth(self):
        digits = 1
        count = max(1, self.blockCount())
        while count >= 10:
            count /= 10
            digits += 1
        space = 3 + self.fontMetrics().width('9') * digits
        return space
            

    # metoda aktualizujaca szerokosc linii numeracji
    # setViewportMargines ustawia marginesy wokol obszaru przewijania
    def updateLineNumberAreaWidth(self, _ ):
        self.setViewportMargins(self.lineNumberAreaWidth(), 0, 0, 0)


    # powiekszenie wysokosci linii, gdy zostanie zawiniety wpisywany tekst
    # aby nie zrobic kolejnej numeracji linii 
    def updateLineNumberArea(self, rect, dy):

        if dy:
            self.lineNumberArea.scroll(0, dy)
        else:
            self.lineNumberArea.update(0, rect.y(), self.lineNumberArea.width(), rect.height())

        # jezeli brak znakow ustaw margines zerowy
        if rect.contains(self.viewport().rect()):
            self.updateLineNumberAreaWidth(0)

    # Zmiana rozmiaru okna powoduje zmiane rozmiaru linii numeracji
    def resizeEvent(self, event):
        super().resizeEvent(event)

        cr = self.contentsRect()
        self.lineNumberArea.setGeometry(QRect(cr.left(), cr.top(), self.lineNumberAreaWidth(), cr.height()))


    # metoda podswietlajaca linie, w ktorej znajduje sie kursor
    def highlightCurrentLine(self):
        extraSelections = []

        if not self.isReadOnly():
            selection = QTextEdit.ExtraSelection()
            lineColor = QColor(Qt.yellow).lighter(160)

            selection.format.setBackground(lineColor)
            selection.format.setProperty(QTextFormat.FullWidthSelection, True)
            selection.cursor = self.textCursor()
            selection.cursor.clearSelection()
            extraSelections.append(selection)
            
        self.setExtraSelections(extraSelections)

    def zerowykursor(self):
        cursor = self.textCursor()
        cursor.movePosition(QTextCursor.Start, QTextCursor.MoveAnchor, 1)
        self.setTextCursor(cursor) 
    
    def nastepnyKursor(self):
        cursor = self.textCursor()
        cursor.movePosition(QTextCursor.NextBlock, QTextCursor.MoveAnchor, 1)
        self.setTextCursor(cursor)
    
    def lineNumberAreaPaintEvent(self, event):
        painter = QPainter(self.lineNumberArea)
        painter.fillRect(event.rect(), Qt.lightGray)

        block = self.firstVisibleBlock()
        blockNumber = block.blockNumber()
        top = int(self.blockBoundingGeometry(block).translated(self.contentOffset()).top())
        bottom = top + int(self.blockBoundingRect(block).height())

        
        while block.isValid() and (top <= event.rect().bottom()):
            if block.isVisible() and (bottom >= event.rect().top()):
                number = str(blockNumber + 1)
                painter.setPen(Qt.black)
                painter.drawText(0, top, self.lineNumberArea.width(), self.fontMetrics().height(), Qt.AlignRight, number)

            block = block.next()
            top = bottom
            bottom = top + int(self.blockBoundingRect(block).height())
            blockNumber += 1


if __name__ == "__main__":
    app = QApplication(sys.argv)

    txt = CodeEditor()
    txt.show()

    sys.exit(app.exec_())