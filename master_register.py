from register import Register


class MasterRegister(Register):
    def __init__(self, size):
        super().__init__(size)
        self.regH = Register(int(size/2))
        self.regL = Register(int(size/2))

    def mov(self, value):
        number_base = self.get_number_base(value)
        int_value = self.convert_to_int(value, number_base)
        if int_value >= 2**self.size:
            self.value = 2 ** self.size - 1
            self.mov_to_registers()
        else:
            self.value = int_value
            self.mov_to_registers()

    def add(self, value):
        number_base = self.get_number_base(value)
        int_value = self.convert_to_int(value, number_base)
        value = self.value + int_value
        if int(value) >= 2 ** self.size:
            self.value = 2 ** self.size - 1
            self.mov_to_registers()
        else:
            self.value = int_value
            self.mov_to_registers()

    def sub(self, value):
        number_base = self.get_number_base(value)
        int_value = self.convert_to_int(value, number_base)
        value = self.value - int_value
        if value <= 0:
            self.value = 0
            self.mov_to_registers()
        else:
            self.value = int_value
            self.mov_to_registers()

    def mov_to_registers(self):
        binary_string = f'{self.value:0{self.size}b}'
        half = int(len(binary_string) / 2)
        old_part = binary_string[:half]
        young_part = binary_string[half:]
        self.regH.value = int(old_part, 2)
        self.regL.value = int(young_part, 2)

    def update_register(self):
        old_part = self.regH.get_value()
        young_part = self.regL.get_value()

        self.value = int(old_part+young_part, 2)
